using System.Collections.Generic;

namespace GradeBook
{
    class Book
    {
        public void AddGrade(double grade)
        {
            grades.Add(grade);
        }

        public void FindAverage(List<double> avg)
        {
            //double average = new double();
            foreach (var number in avg) 
            {
                average += number;
            }
            average /= avg.Count;
        }

        public List<double> grades = new List<double>(){};
        public double average = new double();
    }
    

}