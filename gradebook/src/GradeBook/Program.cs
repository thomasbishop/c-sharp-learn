﻿using System;


namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new Book();
            book.AddGrade(1.5);
            book.FindAverage(book.grades); 
            
            Console.WriteLine(book.average); 
        }
    }
}

/* 

Next challenge:
compute the highest grade and the lowest grade
and print both those with the average to console

hint: use 
var highGrade = double.MinValue;
foreach(var number in grades)
{
    highGrade = Math.Max(number, highGrade);
    result += number;
}
*/
